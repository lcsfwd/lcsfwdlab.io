'use strict';
(function(a) {
	(window.getFloaterPosition = function(a, c, d) {
		var e = 'fixed' == c.css('position'),
			f = a[0].getBoundingClientRect(),
			b = e ? f.top : a.offset().top,
			g = e ? f.left : a.offset().left,
			i = a.outerWidth(),
			j = a.outerHeight(),
			k = c.outerHeight(),
			l =
				document.documentElement.clientWidth ||
				document.body.clientWidth ||
				window.innerWidth,
			m =
				document.documentElement.clientHeight ||
				document.body.clientHeight ||
				window.innerHeight,
			h = {
				top: f.top,
				right: l - f.left - f.width,
				bottom: m - f.bottom - f.height,
				left: f.left
			},
			n = {
				top: 'auto',
				right: 'auto',
				bottom: 'auto',
				left: 'auto',
				distanceFrom: h
			};
		return (
			(n.top = h.top > h.bottom ? b - k + j : b),
			'top' == d && h.top > k + 10 && (n.top = b - k + j),
			'bottom' == d && h.bottom > k + 10 && (n.top = b),
			h.left > h.right ? (n.right = l - (g + i)) : (n.left = g),
			n
		);
	}),
		(a.fn.closeFloater = function(b, c, d) {
			var e = a(b);
			e.is(c) || 0 !== e.has(c).length || a(b).removeClass('active'),
				'function' == typeof d && d.call(e);
		}),
		a(window).on('touchend mouseup blur resize', function(b) {
			a(this).closeFloater('.context-menu, .screen-settings-popup', b.target);
		});
})(jQuery);
