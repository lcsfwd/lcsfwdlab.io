'use strict';
$(document).ready(function(a) {
	a.fn.filterByData = function(b, c) {
		var d = this;
		return 'undefined' == typeof c
			? d.filter(function() {
					return 'undefined' != typeof a(this).data(b);
			  })
			: d.filter(function() {
					return a(this).data(b) == c;
			  });
	};
	var b = function(b) {
		var c = { outter: {}, inner: {} };
		(c.outter.left = '0'),
			(c.outter.right = a(document).width() + 'px'),
			(c.outter.top = '0'),
			(c.outter.bottom = a(document).height() + 'px'),
			(c.inner.left = b.offset().left + 'px'),
			(c.inner.right = b.offset().left + b.outerWidth() + 'px'),
			(c.inner.top = b.offset().top + 'px'),
			(c.inner.bottom = b.offset().top + b.outerHeight() + 'px');
		var d = {
				0: { x: c.outter.left, y: c.outter.top },
				1: { x: c.outter.left, y: c.outter.bottom },
				2: { x: c.inner.left, y: c.outter.bottom },
				3: { x: c.inner.left, y: c.inner.top },
				4: { x: c.inner.right, y: c.inner.top },
				5: { x: c.inner.right, y: c.inner.bottom },
				6: { x: c.inner.left, y: c.inner.bottom },
				7: { x: c.inner.left, y: c.outter.bottom },
				8: { x: c.outter.right, y: c.outter.bottom },
				9: { x: c.outter.right, y: c.outter.top }
			},
			e = '';
		for (var f in d)
			e =
				f < Object.keys(d).length - 1
					? e + d[f].x + ' ' + d[f].y + ', '
					: e + d[f].x + ' ' + d[f].y;
		return console.log(e), e;
	};
	(a.fn.showOverlay = function() {
		var b = a(this),
			c = 'js-overlay',
			d = Math.random()
				.toString(36)
				.slice(2),
			e = a('<div>', { class: c }).data(c, d);
		return (
			b.append(e),
			b.addClass(c + '--active'),
			b.css('height', '100%'),
			(this, e)
		);
	}),
		(a.fn.highlightElement = function(c, d) {
			var f = a(this);
			return (
				null == d && (d = 20),
				f.showOverlay().css('clip-path', 'polygon(' + b(c) + ')'),
				c[0].scrollIntoView({ block: 'start', behavior: 'smooth' }),
				this
			);
		}),
		(a.fn.hideOverlay = function() {
			var b = a(this),
				c = 'js-overlay',
				d = b.data(c),
				e = a(document.body.childNodes).filterByData('jsOverlay', d);
			return e.remove(), b.removeClass(c + '--active'), this;
		});
});
