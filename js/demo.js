'use strict';
var tour = tour || null;
if (null == tour) throw new Error('tour.js is not defined');
$(document).ready(function() {
	for (
		var a,
			b = new tour.Tour(),
			c = ['#one', '#two', '#three', '#four'],
			d = 3e3,
			e = 0;
		e < c.length;
		e++
	)
		(a = c[e]),
			(d += 2e3),
			window.setTimeout(
				function(a) {
					$('body').hideOverlay(), $('body').highlightElement($(a[0]));
				},
				d,
				[a]
			);
});
