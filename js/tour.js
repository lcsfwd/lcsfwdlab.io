(function(global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined'
		? (module.exports = factory())
		: typeof define === 'function' && define.amd
		? define(factory)
		: ((global = global || self), (global.tour = factory()));
})(this, function() {
	'use strict';

	function _classCallCheck(instance, Constructor) {
		if (!(instance instanceof Constructor)) {
			throw new TypeError('Cannot call a class as a function');
		}
	}

	function _defineProperties(target, props) {
		for (var i = 0; i < props.length; i++) {
			var descriptor = props[i];
			descriptor.enumerable = descriptor.enumerable || false;
			descriptor.configurable = true;
			if ('value' in descriptor) descriptor.writable = true;
			Object.defineProperty(target, descriptor.key, descriptor);
		}
	}

	function _createClass(Constructor, protoProps, staticProps) {
		if (protoProps) _defineProperties(Constructor.prototype, protoProps);
		if (staticProps) _defineProperties(Constructor, staticProps);
		return Constructor;
	}

	var TourPoint = function a(b, c) {
		if ((_classCallCheck(this, a), null == c))
			throw new Error('Invalid argument: element can not be null or undefined');
		this.element = c;
	};

	var Tour = (function() {
		function a() {
			_classCallCheck(this, a),
				(this.tourItems = []),
				this.tourItems.push(new TourPoint(1, $('<div>', {}))),
				this.name();
		}
		return (
			_createClass(a, [
				{
					key: 'name',
					value: function name() {
						var a = $('<div>', {});
						(this.tourItems[0].element = a),
							this.tourItems[0].element.ELEMENT_NODE;
					}
				},
				{ key: 'start', value: function start() {} },
				{ key: 'stop', value: function stop() {} }
			]),
			a
		);
	})();

	(function(a) {
		(window.getFloaterPosition = function(a, c, d) {
			var e = 'fixed' == c.css('position'),
				f = a[0].getBoundingClientRect(),
				b = e ? f.top : a.offset().top,
				g = e ? f.left : a.offset().left,
				i = a.outerWidth(),
				j = a.outerHeight(),
				k = c.outerHeight(),
				l =
					document.documentElement.clientWidth ||
					document.body.clientWidth ||
					window.innerWidth,
				m =
					document.documentElement.clientHeight ||
					document.body.clientHeight ||
					window.innerHeight,
				h = {
					top: f.top,
					right: l - f.left - f.width,
					bottom: m - f.bottom - f.height,
					left: f.left
				},
				n = {
					top: 'auto',
					right: 'auto',
					bottom: 'auto',
					left: 'auto',
					distanceFrom: h
				};
			return (
				(n.top = h.top > h.bottom ? b - k + j : b),
				'top' == d && h.top > k + 10 && (n.top = b - k + j),
				'bottom' == d && h.bottom > k + 10 && (n.top = b),
				h.left > h.right ? (n.right = l - (g + i)) : (n.left = g),
				n
			);
		}),
			(a.fn.closeFloater = function(b, c, d) {
				var e = a(b);
				e.is(c) || 0 !== e.has(c).length || a(b).removeClass('active'),
					'function' == typeof d && d.call(e);
			}),
			a(window).on('touchend mouseup blur resize', function(b) {
				a(this).closeFloater('.context-menu, .screen-settings-popup', b.target);
			});
	})(jQuery);

	$(document).ready(function(a) {
		a.fn.filterByData = function(b, c) {
			var d = this;
			return 'undefined' == typeof c
				? d.filter(function() {
						return 'undefined' != typeof a(this).data(b);
				  })
				: d.filter(function() {
						return a(this).data(b) == c;
				  });
		};
		var b = function(b) {
			var c = { outter: {}, inner: {} };
			(c.outter.left = '0'),
				(c.outter.right = a(document).width() + 'px'),
				(c.outter.top = '0'),
				(c.outter.bottom = a(document).height() + 'px'),
				(c.inner.left = b.offset().left + 'px'),
				(c.inner.right = b.offset().left + b.outerWidth() + 'px'),
				(c.inner.top = b.offset().top + 'px'),
				(c.inner.bottom = b.offset().top + b.outerHeight() + 'px');
			var d = {
					0: { x: c.outter.left, y: c.outter.top },
					1: { x: c.outter.left, y: c.outter.bottom },
					2: { x: c.inner.left, y: c.outter.bottom },
					3: { x: c.inner.left, y: c.inner.top },
					4: { x: c.inner.right, y: c.inner.top },
					5: { x: c.inner.right, y: c.inner.bottom },
					6: { x: c.inner.left, y: c.inner.bottom },
					7: { x: c.inner.left, y: c.outter.bottom },
					8: { x: c.outter.right, y: c.outter.bottom },
					9: { x: c.outter.right, y: c.outter.top }
				},
				e = '';
			for (var f in d)
				e =
					f < Object.keys(d).length - 1
						? e + d[f].x + ' ' + d[f].y + ', '
						: e + d[f].x + ' ' + d[f].y;
			return console.log(e), e;
		};
		(a.fn.showOverlay = function() {
			var b = a(this),
				c = 'js-overlay',
				d = Math.random()
					.toString(36)
					.slice(2),
				e = a('<div>', { class: c }).data(c, d);
			return (
				b.append(e), b.addClass(c + '--active'), b.css('height', '100%'), e
			);
		}),
			(a.fn.highlightElement = function(c, d) {
				var f = a(this);
				return (
					f.showOverlay().css('clip-path', 'polygon(' + b(c) + ')'),
					c[0].scrollIntoView({ block: 'start', behavior: 'smooth' }),
					this
				);
			}),
			(a.fn.hideOverlay = function() {
				var b = a(this),
					c = 'js-overlay',
					d = b.data(c),
					e = a(document.body.childNodes).filterByData('jsOverlay', d);
				return e.remove(), b.removeClass(c + '--active'), this;
			});
	});

	var tour_lib = { Tour: Tour, TourPoint: TourPoint };

	return tour_lib;
});
//# sourceMappingURL=tour.js.map
